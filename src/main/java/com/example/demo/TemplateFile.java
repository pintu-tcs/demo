package com.example.demo;

public class TemplateFile extends  TemplateResource {

    public TemplateFile(String name, String template) {
        setName(name);
        setTemplate(template);
    }

    @Override
    public void addChild(String path, String template) {
        throw new UnsupportedOperationException("not supported");
    }

    @Override
    public String getTemplate(String path) {
        if (path.equals(getName())) return getTemplate();
        return null;
    }

    @Override
    public boolean hasChildren() {
        return false;
    }
}
