package com.example.demo;

import java.util.HashSet;
import java.util.Set;

public abstract class TemplateResource implements Comparable<TemplateResource> {
    private String name;
    private String template;
    private Set<TemplateResource> children;

    @Override
    public int compareTo(TemplateResource o) {
        return this.getName().compareTo(o.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public abstract void addChild(String path, String template);

    public abstract String getTemplate(String path);
    public abstract boolean hasChildren();

    public Set<TemplateResource> getChildren() {
        return children;
    }

    public void setChildren(Set<TemplateResource> children) {
        this.children = children;
    }

    public static class Pair<T1, T2> {
        public T1 first;
        public T2 second;

        public Pair(T1 first, T2 second) {
            this.first = first;
            this.second = second;
        }
    }

    public Pair<String, String> getPathComponents(String path) {
        if (path == null || path.isEmpty()) return null;
        final int indexOfSeparator = path.indexOf('/');
        if (indexOfSeparator != -1) {
            String first = path.substring(0, indexOfSeparator);
            String second = path.substring(indexOfSeparator + 1);
            return new TemplateDirectory.Pair<>(first, second);
        } else {
            return new TemplateDirectory.Pair<>(path, null);
        }
    }
}
