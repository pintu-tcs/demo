package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@SpringBootApplication
public class DemoApplication {

	Logger logger = LoggerFactory.getLogger(DemoApplication.class);

	TemplateResource root = null;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	CommandLineRunner initializer() {
		return new CommandLineRunner() {
			@Override
			public void run(String... args) throws Exception {
				PathMatchingResourcePatternResolver patternResolver
						= new PathMatchingResourcePatternResolver();
				final Resource[] resources = patternResolver
						.getResources("classpath*:templates/**/*.json");

				List<String> paths = new ArrayList<>();

				if (resources != null) {
					Arrays.asList(resources).stream()
							.forEach(resource -> {
								String dir = resource.toString().substring(resource.toString().lastIndexOf('!') + 1,
										resource.toString().lastIndexOf(']'));
								try (InputStream inputStream = resource.getInputStream()) {
									String text = new BufferedReader(new InputStreamReader(inputStream,
											StandardCharsets.UTF_8))
											.lines()
											.collect(Collectors.joining("\n"));

									if (root == null) {
										final String modifiedDir = dir.substring(1);
										final int indexOfSeparator = modifiedDir.indexOf('/');
										if (indexOfSeparator != -1) {
											String first = modifiedDir.substring(0, indexOfSeparator);
											root = new TemplateDirectory(first);
											root.addChild(modifiedDir.substring(indexOfSeparator + 1), text);
										}
									} else {
										final String modifiedDir = dir.substring(1);
										final int indexOfSeparator = modifiedDir.indexOf('/');
										if (indexOfSeparator != -1) {
											root.addChild(modifiedDir.substring(indexOfSeparator + 1), text);
										}
									}
									paths.add(dir.substring(1));
								} catch (IOException e) {
									e.printStackTrace();
								}
							});

					paths.forEach(path -> {
						final String template = root.getTemplate(path);
						logger.info("{} -- > {}", path, template);
					});
				}
			}
		};
	}

}
