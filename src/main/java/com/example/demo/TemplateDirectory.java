package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class TemplateDirectory extends TemplateResource {

    Logger logger = LoggerFactory.getLogger(TemplateDirectory.class);

    public TemplateDirectory(String name) {
        setName(name);
    }

    @Override
    public String getTemplate(String path) {
        final Pair<String, String> pathComponents = getPathComponents(path);
        if (pathComponents == null) return null;

        String parent = pathComponents.first;
        if (parent.equals(getName())) {
           if (hasChildren()) {
               for (TemplateResource tr : getChildren()) {
                   final Pair<String, String> childComponents = getPathComponents(pathComponents.second);
                   if (childComponents != null) {
                       if (childComponents.first.equals(tr.getName())) {
                           return tr.getTemplate(pathComponents.second);
                       }
                   }
               }
           }
        }
        return null;
    }

    public void addChild(String path, String template) {
        final Pair<String, String> pathComponents = getPathComponents(path);
        if (pathComponents != null) {
            if (getChildren() == null) {
                setChildren(new HashSet<>());
            }
            if (pathComponents.second != null) {
                final Set<String> children = getChildren().stream().map(TemplateResource::getName).collect(Collectors.toSet());
                if (!children.contains(pathComponents.first)) {
                    final TemplateDirectory templateDirectory = new TemplateDirectory(pathComponents.first);
                    getChildren().add(templateDirectory);
                    templateDirectory.addChild(pathComponents.second, template);
                } else {
                    final TemplateResource templateResource = getChild(getChildren(), pathComponents.first);
                    if (templateResource != null) {
                        templateResource.addChild(pathComponents.second, template);
                    }
                }
            } else {
                final TemplateFile templateFile = new TemplateFile(pathComponents.first, template);
                getChildren().add(templateFile);
            }
        }
    }

    private TemplateResource getChild(Set<TemplateResource> children, String searchString) {
        if (children == null || children.isEmpty()) return null;
        for (TemplateResource tr : children) {
            if (tr.getName().equals(searchString)) return tr;
        }
        return null;
    }

    @Override
    public boolean hasChildren() {
        return getChildren() != null && !getChildren().isEmpty();
    }

}
